
let googleKey = 'AIzaSyD1XasGU85Aw0gXuWSx1PzZ4RWOJ-arTuI';

let googleClient = require('\@google/maps').createClient({
  key: googleKey
});

let prettyjson  = require('prettyjson');
let request = require('request');
let dbController = require('../models/controller');
let shortid = require('shortid');
let fs = require('fs');


exports.searchPlace = (searchQuery,latlngObj,cb)=>{
	console.log("about to search something");
	googleClient.places({query:searchQuery,location:latlngObj},(err,response)=>{
		if(err)console.log(err)
		else cb(response.json.results);
	});
}

exports.getPlaceDetails = (placeid,cb)=>{
	console.log("About to get details for  "+placeid);
	googleClient.place({placeid:placeid},(err,response)=>{
		if(err)console.log(err);
		else cb(response.json.result.reviews);

		
	});
}

exports.getLocation = (address,cb) =>{

	googleClient.geocode({address:address},(err,response)=>{
		if(err)console.log(err);
		else cb(response["json"]["results"][0]["geometry"]["location"]);
	});


}

exports.getImageUrl = (photoreference,cb)=>{

	if(photoreference==null){
		cb(null);
		return;
	}
	dbController.getImageId(photoreference,(err,id)=>{
		if(err){
			let id = shortid.generate();
			let options = { method: 'GET',
		  		url: 'https://maps.googleapis.com/maps/api/place/photo',
		  		qs: {
		   			photoreference: photoreference,
		     		key: googleKey,
		     		maxwidth:500
		     	},
		   };
		request(options, function (error, response, body) {
		  		if (error) throw new Error(error);
			}).pipe(fs.createWriteStream(__dirname+'/../public/images/'+id+'.jpg').on('close',function(){
				dbController.setImageId(photoreference,id,()=>{
					cb('images/'+id+'.jpg');
				});
			}));
		}

		else cb('images/'+id+'.jpg');
	});
}




