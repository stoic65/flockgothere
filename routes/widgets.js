
let placesController = require("../apiControllers/googlePlaces");
var express = require("express");
let prettyjson  = require('prettyjson');
var api = express.Router();
let flock = require('flockos');
let dbController = require('../models/controller');
let config = require('../config.js');
let async = require('async');

let url = require('url');
let querystring = require('querystring');




api.get("/widget/chooseDestination", function(req, res) {
	
	let flockEvent = JSON.parse(req.query.flockEvent);
	if(!flockEvent["text"]){
		res.end("No place query was found");
		return;
	}
	let destination = flockEvent["text"].split(":")[0];
	let userLocation = flockEvent["text"].split(":")[1];
	if(userLocation==undefined||userLocation == null)
	{
		dbController.getUser(flockEvent["userId"],(userObj)=>{
			placesController.searchPlace(destination,{"lat":userObj["lat"],"lng":userObj["lon"]},(result)=>{
				res.render('chooseDestination',{res:result,flockEvent :flockEvent });
			});
		});

	}
	else 
	{
		placesController.getLocation(userLocation,function(latLonObj){
			dbController.updateLocation(flockEvent["userId"],latLonObj,userLocation,(userObj)=>{
				placesController.searchPlace(destination,{"lat":userObj["lat"],"lng":userObj["lon"]},(result)=>{
					res.render('chooseDestination',{res:result,flockEvent :flockEvent });	
				});
			});
		});
	}

});


api.post("/widget/gotDestination",(req,res,next)=>{
	
	let placeObj = JSON.parse(req.body.placeObj);
	dbController.getUser(req.body.userid,(userObject)=>{
		placesController.getImageUrl(placeObj["photos"]?placeObj["photos"][0]["photo_reference"]:null,(url)=>{
			dbController.createAttachment(req.body.userid,req.body.chat,url,placeObj,(attachmentObj)=>{
				dbController.addAttachmentToUser(req.body.userid,attachmentObj["_id"],(err)=>{
					if(err)console.log(err);
					flock.callMethod('chat.sendMessage',userObject.token,{
						to:req.body.chat,
						text:"Your request for place search",
						attachments:[{
							"id":attachmentObj["_id"],
							"title":attachmentObj["name"],
							"description":attachmentObj["address"],
							"views":{
								"widget": { "src": "https://5d57321d.ngrok.io/widgets/attachment" ,"height":480,"width":500},
								"image":{
									"original":{"src":"https://5d57321d.ngrok.io/"+attachmentObj["imageUrl"]}
								}
							},
							"buttons": [ {
						        "name": "View Reviews",
						        "icon": "",
						        "action": { "type": "openWidget","desktopType":"sidebar","mobileType":"modal","url":"https://5d57321d.ngrok.io/widgets/getReviews" },
						        "id": "View Reviews"
						    },
						    {
						        "name": "Get Directions",
						        "icon": "",
						        "action": { "type": "openWidget","desktopType":"modal","mobileType":"modal","url":"https://5d57321d.ngrok.io/widgets/getDirections"},
						        "id": "Get direction"
						    }]
						}]
					},(response)=>{
						console.log(response);
					});

				});

				


			});
		});
		res.end();

	});	
	

});

api.get("/widgets/attachment",(req,res,next)=>{
	console.log("get attachment");	let FlockEvent = JSON.parse(req.query.flockEvent);
	dbController.getAttachment(FlockEvent["attachmentId"],(attachmentObj)=>{
		let ownerId = attachmentObj["userid"];
		dbController.getUser(ownerId,(userObj)=>{
			console.log("inside getuser");
			console.log(attachmentObj["upvoted"]);
			async.map(attachmentObj["upvoted"],(item,callback)=>{
				console.log("hiya");
				console.log(item);
				flock.callMethod("users.getPublicProfile",userObj.token,{
					"userId":item
				},(err,response)=>{
					console.log(response);
					callback(null,response);
				});
			},(err,publicProfilesArray)=>{
				if(err)console.log(err);
				let isInterested = attachmentObj["upvoted"].indexOf(FlockEvent.userId)>-1?true:false;
				res.render("mainAttachment",{
					"attachmentObj":attachmentObj,
					"publicProfilesArray":publicProfilesArray,
					"isInterested":isInterested
				});

			});

		});





	});





	//res.render("mainAttachment",{imageUrl:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTS1hevM-88hmiqyYOeeCeVUTzCS63jl31LEyLAIjY7AKWXDVe"});
});









api.get("/widgets/getReviews",(req,res,next)=>{

	let flockEvent = JSON.parse(req.query.flockEvent);
	dbController.getAttachment(flockEvent["attachmentId"],(attachmentObj)=>{
		placesController.getPlaceDetails(attachmentObj["placeid"],(results)=>{
			res.render("reviews",{results:results});
		});
	});

});


api.get("/widgets/getDirections",(req,res,next)=>{
	let flockEvent = JSON.parse(req.query.flockEvent);
	dbController.getUser(flockEvent["userId"],(userObj)=>{
		let userLocation = "NewDelhi";
		if(userObj!=null) userLocation = userObj["location"];
		dbController.getAttachment(flockEvent["attachmentId"],(attachmentObj)=>{
			res.render("directions",{origin:userLocation,destination:attachmentObj["placeid"]});
		});

	});


});

api.get('/widgets/appLaunch',(req,res,next)=>{
	console.log(req.query);
	let FlockEvent = JSON.parse(req.query.flockEvent);
	dbController.getUser(FlockEvent.userId,(userObject)=>{
		async.map(userObject.attachments,(item,callback)=>{
			dbController.getAttachment(item,(attachmentObj)=>{
				let userProfilesArray = [];
				async.map(attachmentObj["upvoted"],(item2,callback2)=>{
					flock.callMethod("users.getPublicProfile",userObject.token,{
						"userId":item2
					},(err,response)=>{
						console.log(response);
						callback2(null,response);
					});
				},(err,publicProfilesArray)=>{
					if(err)console.log(err);
					userProfilesArray = publicProfilesArray;
					callback(null,{"attachment":attachmentObj,"userProfiles":userProfilesArray});
				});

			});
		},(err,resultArray)=>{
			if(err)console.log(err);
			res.render("mainApp",{"results":resultArray});
		})
	});



});

api.post("/widgets/attachmentToggle",(req,res,next)=>{
	let query = querystring.parse(url.parse(req.headers.referer).query);
	console.log(query);
	let FlockEvent = JSON.parse(query.flockEvent);
	if(req.body.isChecked=="true")
	{
		dbController.addUserToAttachment(FlockEvent.userId,FlockEvent.attachmentId,()=>{
			res.end("Done");
		});
	}
	else
	{
		dbController.removeUserFromAttachment(FlockEvent.userId,FlockEvent.attachmentId,()=>{
			res.end("Done");
		});
	}
});















module.exports  = api;