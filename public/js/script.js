$(document).ready(function(){

	$(".placeCard").on('click',function(event){
		event.preventDefault();
		var thisCard = $(this);
		$.post("/widget/gotDestination",{
			userid:$("#userDetail").attr("data-username"),
			chat:$("#userDetail").attr("data-chat"),
			placeObj:thisCard.attr("data-placeObj")
			
		},function(data){
			flock.close();
		});

	});	

	$("#submitLocation").on("click",function(e){
		e.preventDefault();
		var userid = $("#userDiv").attr("data-userid");
		var userLocation = $('#location').val();
	
		if(userLocation == "" ||userLocation == null || userLocation.length==0)
			Materialize.toast("Location cannot be empty",2000);
		else{
			$.post("/configuration/locationSet",{"location":userLocation,"userid":userid},function(data){
				Materialize.toast("Location Set to "+userLocation, 2000);	
			});
		}
	});

	$("#interested").change(function(e){
		e.preventDefault();
		var isChecked = this.checked;
		$.post('/widgets/attachmentToggle',{
			"isChecked":isChecked
		},function(data){
			location.reload(true);
		});
	});	

});