'use strict';
let flock = require('flockos');
let express = require('express');
let fs = require('fs');
let config = require('./config.js');
let prettyjson = require('prettyjson');
let bodyParser = require("body-parser");

flock.setAppId(config.appId);
flock.setAppSecret(config.appSecret);

let app = express();

app.set("view engine", "ejs");
app.set("views", __dirname+ "/views");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname+"/public"));


let dbController = require('./models/controller');

let widgetRouter = require('./routes/widgets');

let configurationRouter = require('./routes/configuration');




app.use('/configuration',configurationRouter);
app.use(widgetRouter);
app.use('/',flock.events.tokenVerifier);


app.post('/events', flock.events.listener);

// save tokens on app.install
flock.events.on('app.install', (event)=>{
	console.log("App install event ");
	console.log(event);
	flock.callMethod("users.getInfo",event.token,{},(err,userObject)=>{
    	if(err)console.log(err);
    	dbController.createUser(event,userObject,()=>{
    		console.log("User created");
    	});
    });
});





flock.events.on('client.slashCommand',(event)=>{
	console.log(event)
	if(!event.text)return{
		text:"Please enter some text"
	}






});


flock.events.on('app.uninstall', (event)=>{
	dbController.deleteUser(event.userId,()=>{
		console.log("User deleted");
	});

});

flock.events.on('client.flockmlAction',(event)=>{
	console.log(event);
});




var port = 3000;
app.listen(port, ()=>{
    console.log('Listening on port: ' + port);
});

// exit handling -- save tokens in token.js before leaving
process.on('SIGINT', process.exit);
process.on('SIGTERM', process.exit);
process.on('exit', function () {

});