let mongoose = require('mongoose');

let attachmentSchema  = new mongoose.Schema({
	"userid":String,
	"chat":String,
	"imageUrl":String,
	"address":String,
	"name":String,
	"placeid":String,
	"rating":Number,
	"attachmentId":String,
	"upvoted":[String],
	"upvote_count":{type:Number,default:0}
});

mongoose.model("Attachment",attachmentSchema);