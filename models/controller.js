let mongoose = require('mongoose');

mongoose.connect("mongodb://localhost/flock",(err)=>{
	if(err)console.log(err);
	else console.log("Connected to db");

});

require('./attachment.js');
require('./user.js');
require('./places.js');
require("./imagesMapper.js");
let shortid = require('shortid');
let User = mongoose.model('User');
let Attachment = mongoose.model("Attachment");
let Place = mongoose.model("Place");
let ImageMapper = mongoose.model("ImageMapper");


exports.deleteUser = (userid,cb)=>{
	User.findOne({"userid":userid},(err,usertoDelete)=>{
		usertoDelete.remove((err)=>{
			if(err)console.log(err);
			else cb();
		});
	});
}

exports.createUser = (event,userObject,cb)=>{
	User.create({
    	"userid":event.userId,
    	"token":event.token,
    	"teamId":userObject.teamId,
    	"firstName":userObject.firstName,
   		"lastName":userObject.lastName,
    	"role":userObject.role,
    	"profileImage":userObject.profileImage
    },(err,user)=>{
    	if(err)console.log(err);
    	else cb();
   	});
}

exports.addPlace = (placeObj,cb)=>{
	Place.create({
		"placeid":placeObj["place_id"],
		"name":placeObj["name"],
		"address":placeObj["formatted_address"],
		"lat":placeObj["geometry"]["location"]["lat"],
		"lon":placeObj["geometry"]["location"]["lng"],
		"icon":placeObj["icon"],
		"rating":placeObj["rating"],
		"types":placeObj["types"]
	},(err,user)=>{
		if(err)console.log(err);
		else cb();
	});
}

exports.getPlace = (placeid,cb)=>{
	Place.findOne({"placeid":placeid},(err,placeObj)=>{
		if(err||placeObj==null)
		{
			cb(null);
		}
		else cb(placeObj);
	});
}

exports.updateLocation = (userid,latlonobj,userLocation,cb)=>{

	

	User.findOne({'userid':userid},(err,userObj)=>{
		if(err)console.log(err);
		userObj["lat"] = latlonobj["lat"];
		userObj["lon"] = latlonobj["lng"];
		userObj["location"] = userLocation;
		userObj.save((err,user)=>{
			if(err)console.log(err);
			else cb(user);
		})
	});
}

exports.setImageId = (photoreference,shortId,cb)=>{

	ImageMapper.create({
		shortId:shortId,
		photoreference:photoreference
	},(err,returnObj)=>{
		if(err)console.log(err);
		else cb();
	})
}

exports.getImageId = (photoreference,cb)=>{
	ImageMapper.findOne({'photoreference' :photoreference},(err,imageMapperObj)=>{
		console.log("sup m8");
		console.log(err);
		console.log(imageMapperObj);
		console.log("sup m9");
		if(err||imageMapperObj==undefined)
			cb(new Error("Not found"),null);
		else cb(null,imageMapperObj["shortId"]);
	});
}


exports.getUser = (userid,cb)=>{
	User.findOne({'userid':userid},(err,userObj)=>{
		if(err||userObj==null)cb(null);
		else cb(userObj);
	});

}
exports.createAttachment = (userid,chat,imageUrl,placeObj,cb)=>{
	let attachmentId = shortid.generate();
	Attachment.create({
		userid:userid,
		chat:chat,
		imageUrl:imageUrl,
		address:placeObj["formatted_address"],
		name:placeObj["name"],
		placeid:placeObj["place_id"],
		rating:placeObj["rating"],
		attachmentId:attachmentId
	},(err,attachmentObj)=>{
		if(err)console.log(err);
		cb(attachmentObj);

	});

}


exports.getAttachment = (id,cb)=>{
	Attachment.findOne({"_id":id},(err,response)=>{
		cb(response);
	});
}

exports.addAttachmentToUser = (userid,attachmentId,cb)=>{
	User.findOne({'userid':userid},(err,userObject)=>{
		if(err)cb(err);
		else{
			userObject["attachments"].push(attachmentId);
			userObject.save((err,userObj)=>{
				cb(null);
			});
		}
	});
};

exports.addUserToAttachment = (userid,attachmentId,cb)=>{
	Attachment.findOne({"_id":attachmentId},(err,attachmentObj)=>{
		if(err)console.log(err);
		attachmentObj["upvoted"].push(userid);

		attachmentObj["upvote_count"]=attachmentObj["upvote_count"]+1;
		attachmentObj.save((err,attachmentObj)=>{
			console.log(attachmentObj);
			cb();
		});
	});

}
exports.removeUserFromAttachment = (userid,attachmentId,cb)=>{
	Attachment.findOne({"_id":attachmentId},(err,attachmentObj)=>{
		if(err)console.log(err);
		let index = attachmentObj["upvoted"].indexOf(userid);
		console.log(userid);
		console.log("index",index);
		if(index>-1)
		{
			attachmentObj["upvoted"].splice(index,1);
			//console.log(attachmentObj["upvoted"]);
		}
		attachmentObj["upvote_count"]=attachmentObj["upvote_count"]-1;
		attachmentObj.save((err,attachmentObj)=>{
			console.log(attachmentObj);
			cb();
		});
	});	
}




