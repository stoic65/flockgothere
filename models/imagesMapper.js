let mongoose = require('mongoose');

let imageMapperSchema  = new mongoose.Schema({
	"shortId":String,
	"photoreference":String
});

mongoose.model("ImageMapper",imageMapperSchema);