let mongoose = require('mongoose');

let placeSchema  = new mongoose.Schema({
	"placeId":{type:String,unique:true},
	"name":String,
	"address":String,
	"lat":Number,
	"lon":Number,
	"icon":String,
	"rating":Number,
	"types":[String]
});

mongoose.model("Place",placeSchema);