let mongoose = require('mongoose');

let userSchema  = new mongoose.Schema({
	"userid":{type:String, unique:true},
	"token":String,
	"lat":{type:String,default:"28.6139"},
	"lon":{type:String,default:"77.2090"},
	"location":{type:String,default:"New Delhi"},
	"teamId":String,
	"email":String,
	"firstName":String,
	"lastName":String,
	"role":String,
	"profileImage":String,
	"attachments":[String],
	"uber_acces_toke":String,
	"uber_refresh_token":String
});

mongoose.model("User",userSchema);